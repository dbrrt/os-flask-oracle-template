from flask import Flask, session
from flask_restful import Api
from api.v1 import users

from flask_cors import CORS

app = Flask(__name__)
# Enabling CORS
CORS(app, resources={r"/api/v1/*": {"origins": "*"}})
# API Instantiation
api = Api(app)
# Users Endpoint
api.add_resource(users.UsersCtrl, '/api/v1/users')

@app.route('/')
def hello_world():
    return 'API reachable following the pattern /api/v1/*'

if __name__ == '__main__':
    app.run(threaded=True)
