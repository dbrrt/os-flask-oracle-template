from flask_restful import Resource, reqparse

class UsersCtrl(Resource):
    # /v1/api/users
    # will return an array of users (JSON objects)
    def get(self):
        #import cx_Oracle
        users = [{'name': 'David'}, {'name': 'Octave'}]
        return users
