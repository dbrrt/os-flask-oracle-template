from setuptools import setup

setup(name='FlaskOpenShift',
      version='1.0',
      description='Flask App',
      author='David Barrat',
      author_email='david.barrat.1@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/'
     )
