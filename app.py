#!/usr/bin/python
# import os
#
# virtenv = os.environ['OPENSHIFT_PYTHON_DIR'] + '/virtenv/'
# # virtualenv = os.path.join(virtenv, 'bin/activate_this.py')
# # try:
# #     execfile(virtualenv, dict(__file__=virtualenv))
# # except IOError:
# #     pass
#
# IMPORTANT: Put any additional includes below this line.  If placed above this
# line, it's possible required libraries won't be in your searchable path
#

from flaskapp import app as application

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    DEFAULT_PORT = 8080
    httpd = make_server('0.0.0.0', 8080, application)
    httpd.serve_forever()
